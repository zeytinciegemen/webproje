﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebProje.Data;

namespace WebProje.Areas.Yonetici.Controllers
{
    public class YoneticiController : Controller
    {

        WebProjeContext vt = new WebProjeContext();

        public ActionResult Index()
        {
            if ((string)Session["Yetki"] == "Yonetici")
            {
                return View();
            }
            return View("YetkiYok");
        }

        public ActionResult UyeIslemleri()
        {
            if ((string)Session["Yetki"] == "Yonetici")
            {
                var model = vt.Uyes.ToList();
                return View(model);
            }
            return View("YetkiYok");
        }

        public ActionResult KitapIslemleri()
        {
            if ((string)Session["Yetki"] == "Yonetici")
            {
                var model = vt.Kitaps.ToList();
                return View(model);
            }
            return View("YetkiYok");
        }

        public ActionResult Detay(int id)
        {
            if ((string)Session["Yetki"] == "Yonetici")
            {
                var model = vt.Uyes.Find(id);
                return View(model);
            }
            return View("YetkiYok");
        }

        public ActionResult UyeSil(int id)
        {
            if ((string)Session["Yetki"] == "Yonetici")
            {
                var model = vt.Uyes.Find(id);
                vt.Uyes.Remove(model);
                vt.SaveChanges();
                return View("Index", vt.Uyes);
            }
            return View("YetkiYok");
        }

        public ActionResult Duzenle(int id)
        {
            if ((string)Session["Yetki"] == "Yonetici")
            {
                var model = vt.Uyes.Find(id);
                return View(model);
            }
            return View("YetkiYok");
        }

        public ActionResult UyeEkle()
        {
            if ((string)Session["Yetki"] == "Yonetici")
            {
                return View();
            }
            return View("YetkiYok");
        }

        [HttpPost]
        public ActionResult UyeEkle(Uye uye)
        {
            if ((string)Session["Yetki"] == "Yonetici")
            {
                vt.Uyes.Add(uye);
                vt.SaveChanges();
                return View("Index", vt.Uyes);
            }
            return View("YetkiYok");
        }

        public ActionResult KitapDetay(int id)
        {
            if ((string)Session["Yetki"] == "Yonetici")
            {
                var model = vt.Kitaps.Find(id);
                return View(model);
            }
            return View("YetkiYok");
        }

        public ActionResult KitapEkle()
        {
            if ((string)Session["Yetki"] == "Yonetici")
            {
                return View();
            }
            return View("YetkiYok");
        }

        [HttpPost]
        public ActionResult KitapEkle(Kitap kitap)
        {
            if ((string)Session["Yetki"] == "Yonetici")
            {
                vt.Kitaps.Add(kitap);
                vt.SaveChanges();
                return View("Index", vt.Kitaps);
            }
            return View("YetkiYok");
        }

        public ActionResult KitapSil(int id)
        {
            if ((string)Session["Yetki"] == "Yonetici")
            {
                var model = vt.Kitaps.Find(id);
                vt.Kitaps.Remove(model);
                vt.SaveChanges();
                return View("Index", vt.Kitaps);
            }
            return View("YetkiYok");
        }

        public ActionResult KitapDuzenle(int id)
        {
            if ((string)Session["Yetki"] == "Yonetici")
            {
                var model = vt.Kitaps.Find(id);
                return View(model);
            }
            return View("YetkiYok");
        }
    }
}
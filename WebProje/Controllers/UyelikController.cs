﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebProje.Data;
using WebProje.Models;

namespace WebProje.Controllers
{
    public class UyelikController : Controller
    {
        public ActionResult YeniUyelik()
        {
            return View();
        }

        [HttpPost]
        public ActionResult YeniUyelik(Uye model, string textBoxDogum, HttpPostedFileBase file)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }
            if (String.IsNullOrEmpty(textBoxDogum))
            {
                ModelState.AddModelError("textBoxDogum", "Doğum tarihi boş geçilemez");
                return View();
            }

            int yil = int.Parse(textBoxDogum.Substring(6));

            if (yil > 2002)
            {
                ModelState.AddModelError("textBoxDogum", "Yaşınız 12 den küçük olamaz");
                return View();
            }

            Uye uye = new Uye();
            if (file != null)
            {
                file.SaveAs(Server.MapPath("~/Images/") + file.FileName);
                uye.uyeResim = "/Images/" + file.FileName;
            }

            uye.uyeAd = model.uyeAd;
            uye.EPosta = model.EPosta;
            uye.uyeSoyad = model.uyeSoyad;
            uye.UyeOlmaTarih = DateTime.Now;
            uye.Sifre = model.Sifre;

            using (WebProjeContext vt = new WebProjeContext())
            {
                vt.Uyes.Add(uye);
                vt.SaveChanges();
                return RedirectToAction("Anasayfa","Home");
            }
        }

        public ActionResult Giris()
        {
            return View();
        }

        [HttpPost]
        public string UyeGiris()
        {

            string posta = Request.Form["txtPosta"];
            string sifre = Request.Form["pswSifre"];
            if (String.IsNullOrEmpty(posta) && String.IsNullOrEmpty(sifre))
            {
                return "E-Posta adresinizi ve şifrenizi girmediniz.";
            }
            else if (String.IsNullOrEmpty(posta))
            {
                return "E-posta adresinizi girmediniz.";
            }
            else if (string.IsNullOrEmpty(sifre))
            {
                return "Şifrenizi girmediniz.";
            }
            else
            {
                using (WebProjeContext db = new WebProjeContext())
                {
                    var uye = (from i in db.Uyes where i.Sifre == sifre && i.EPosta == posta select i).SingleOrDefault();
                    
                    if (uye == null) return "Kullanıcı adınızı ya da şifreyi hatalı girdiniz.";

                    Session["uyeAd"] = uye.uyeAd;
                    if(uye.Yetki!=null)
                    Session["yetki"] = uye.Yetki.yetkiAdi;

                    return "Sisteme, başarıyla giriş yaptınız.<script type='text/javascript'>setTimeout(function(){window.location='/Home/Anasayfa'},1000);</script>";
                }
            }
        }

        public ActionResult Cikis()
        {
            Session.Clear();
            return RedirectToAction("Anasayfa","Home");
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using WebProje.Data;

namespace WebProje.Controllers
{
    public class HomeController : Controller
    {
        Uye uye;
        WebProjeContext vt = new WebProjeContext();

        public ActionResult Anasayfa()
        {
            List<Kitap> kitapListe = vt.Kitaps.OrderByDescending(x => x.kitapTarih).Take(10).ToList();
            return PartialView(kitapListe);
        }
        
        public ActionResult Hakkimizda()
        {
            return View();
        }

        public ActionResult Iletisim()
        {
            return View();
        }
        
        public ActionResult PuanKatalogu()
        {
            return View();
        }
        
        public ActionResult Arama(string aranan)
        {
            List<Kitap> aramaListesi = vt.Kitaps.ToList();

            if (!String.IsNullOrEmpty(aranan))
            {
                aramaListesi = vt.Kitaps.Where(x => x.kitapAd.Contains(aranan)).ToList();
            }

            return PartialView(aramaListesi);
        }
        
        public ActionResult Roman()
        {
            List<Kitap> romanListesi = vt.Kitaps.Where(x => x.kategori == "Roman").ToList();
            return PartialView(romanListesi);
        }

        public ActionResult Oyku()
        {
            List<Kitap> oykuListesi = vt.Kitaps.Where(x => x.kategori == "Öykü").ToList();
            return PartialView(oykuListesi);
        }

        public ActionResult Siir()
        {
            List<Kitap> siirListesi = vt.Kitaps.Where(x => x.kategori == "Şiir").ToList();
            return PartialView(siirListesi);
        }

        public ActionResult Deneme()
        {
            List<Kitap> denemeListesi = vt.Kitaps.Where(x => x.kategori == "Deneme").ToList();
            return PartialView(denemeListesi);
        }

        public ActionResult Ani()
        {
            List<Kitap> aniListesi = vt.Kitaps.Where(x => x.kategori == "Anı").ToList();
            return PartialView(aniListesi);
        }

        public ActionResult GeziYazisi()
        {
            List<Kitap> geziListesi = vt.Kitaps.Where(x => x.kategori == "Gezi Yazısı").ToList();
            return PartialView(geziListesi);
        }

        public ActionResult CizgiRoman()
        {
            List<Kitap> cizgiListesi = vt.Kitaps.Where(x => x.kategori == "Çizgi Roman").ToList();
            return PartialView(cizgiListesi);
        }

        public ActionResult Arastirma()
        {
            List<Kitap> arastirmaListesi = vt.Kitaps.Where(x => x.kategori == "Araştırma" || x.kategori == "Tarih").ToList();
            return PartialView(arastirmaListesi);
        }


        public ActionResult ChangeCulture(string lang, string returnUrl)
        {
            Session["Culture"] = new CultureInfo(lang);
            return Redirect(returnUrl);
        }
    }
}
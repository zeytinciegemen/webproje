﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebProje.Data;

namespace WebProje.Controllers
{
    public class CRUDController : Controller
    {
        WebProjeContext vt = new WebProjeContext();

        public ActionResult Detay()
        {
            List<Yorum> yorumListe = vt.Yorums.OrderByDescending(x => x.yorumTarih).ToList();
            return PartialView(yorumListe);
        }
    }
}

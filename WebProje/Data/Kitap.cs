﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebProje.Data
{
    public class Kitap
    {
        public int kitapID { get; set; }

        [Required(ErrorMessage = "Lütfen kitabın adını giriniz.")]
        [StringLength(50, MinimumLength = 3, ErrorMessage = "Kitabın adı 3-50 karakter arasında olmalıdır.")]
        public string kitapAd { get; set; }

        [Required(ErrorMessage = "Lütfen kitabın yazarını giriniz.")]
        [StringLength(50, MinimumLength = 3, ErrorMessage = "Kitabın yazarı 3-50 karakter arasında olmalıdır.")]
        public string kitapYazar { get; set; }

        [Required(ErrorMessage = "Lütfen kategori giriniz.")]
        [StringLength(50, MinimumLength = 3, ErrorMessage = "Kategori alanı 3-50 karakter arasında olmalıdır.")]
        public string kategori { get; set; }

        [Required(ErrorMessage = "Lütfen kitabın eklenme tarihini giriniz.")]
        [DataType(DataType.Date, ErrorMessage = "Lütfen kitabın eklenme  tarihini, doğru bir şekilde giriniz.")]
        public DateTime kitapTarih { get; set; }

        [DataType(DataType.ImageUrl, ErrorMessage = "Lütfen resim yolunuzu doğru şekilde giriniz.")]
        public string kitapResim { get; set; }

        public virtual Uye Uye { get; set; }
        public virtual List<Yorum> Yorums { get; set; }
    }
}
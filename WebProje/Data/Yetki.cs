﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebProje.Data
{
    public class Yetki : AuthorizeAttribute
    {
        public int yetkiID { get; set; }
        public string yetkiAdi { get; set; }
    }
}
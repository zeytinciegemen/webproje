﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;
using WebProje.Models;

namespace WebProje.Data
{
    public class WebProjeContext : DbContext
    {
        public DbSet<Uye> Uyes { get; set; }
        public DbSet<Kitap> Kitaps { get; set; }
        public DbSet<Yorum> Yorums { get; set; }
        public DbSet<Yetki> Yetkis { get; set; }
    }
}
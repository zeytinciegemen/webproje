﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebProje.Data
{
    public class Yorum
    {
        public int yorumID { get; set; }

        [Required(ErrorMessage = "Lütfen yorumunuzu giriniz.")]
        public string yorumIcerik { get; set; }

        [Required(ErrorMessage = "Lütfen yorumun yapılma tarihini giriniz.")]
        [DataType(DataType.Date, ErrorMessage = "Lütfen yorum yapılma tarihini, doğru bir şekilde giriniz.")]
        public DateTime yorumTarih { get; set; }

        public virtual Kitap Kitaplar { get; set; }
        public virtual Uye Uyeler { get; set; }
    }
}